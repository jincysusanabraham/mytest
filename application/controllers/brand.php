<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class brand extends CI_Controller{

	function __construct(){
		parent::__construct();
	   $this->load->model('brand_model','item_model',TRUE);
	     $this->load->library(array('form_validation'));	
	     		$this->load->helper(array('form', 'url','file'));

		
	}
	
		
	function index($row=0){
	
		

 	//$data['query'] = $this->item_model->get_all_entries();
	
    //$this->load->view('academics/index', $entries);
		$limit=NULL;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'brand/index/';
		$config['full_tag_open'] = '<li>';
        $config['full_tag_close'] = '</li>';
		$config['uri_segment'] = 4;
		$condition=array();
		$total=$this->item_model->countrows();
		$config['total_rows']=$total;
		$data['total_rows']=$total;
		$config['per_page']=$limit;
		$this->pagination->initialize($config);
		$data['links']=$this->pagination->create_links();
		if($this->session->userdata('user_type')==1){	
		$data['brand']=$this->item_model->get_all_entries($row,$limit,$condition,'created_date','asc');
		$data['media']=$this->item_model->get_media();
		}else
		{
		$data['brand']=$this->item_model->get_all_entries($row,$limit,$condition,'created_date','asc');
		$data['media']=$this->item_model->get_media();
		}
		$data['row']=$row;
	
			$this->load->view('header', $data);
         //   $this->load->view('admin/left');
            $this->load->view('brand_list');
            $this->load->view('footer');

		
	}
	
	public function show_add()
	{
	

	     $this->load->view('header');
         $this->load->view('add_brand');
	     $this->load->view('footer');	

	}
	
	public function add()
	{
		$this->form_validation->set_rules('brand_name','Name','required');
		$this->form_validation->set_rules('b_img', 'Profile Image', 'callback_image_upload');
		// $this->form_validation->set_rules('phone','Phone','required|integer');
	   // $this->form_validation->set_rules('b_img','Image','required');
		//$this->form_validation->set_rules('img','Image','required');


		if($this->form_validation->run() == TRUE)
		{
		$this->load->helper(array('form', 'url','file'));
		//$this->load->helper(array('form', 'url'));
			 $config['upload_path'] = './uploads/brand';
		//$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|txt|docx';//txt,doc,docx,pdf,png,jpeg,jpg,gif

		$config['max_size']      = 2048;//MB
		$this->load->library('upload', $config);

        if (!$this->upload->do_upload('b_img'))
		 {
		$error =$this->upload->display_errors();
		//$this->load->view('add_brand', $error); 						
		} else {
		
        	$file_data=$this->upload->data();
        	//$da=base_url().'/uploads/'.$file_data['file_name'];//url
			//$da1='/uploads/'.$file_data['file_name'];//path
			//$da2=$file_data['file_name'];//file name
        	$img_path='/uploads/brand/'.$file_data['file_name'];//path
			}
$today = date("Y-m-d");

			$data	=	array(
			'posted_date'=>$today,
			'm_img'=> $img_path,

			 );


		$this->load->model('item_model');
		$image_id=$this->item_model->add_image($data);
	
	$datas	=	array('brand_name'=>$this->input->post('brand_name'),
			              'created_date'=>$today,	
			             // 'b_desc'=>$this->input->post('b_desc'),
			              'b_img'=>$image_id,
			              
			                );
		$this->load->model('item_model');
		$this->item_model->add_data($datas);
		
		redirect('brand');	
	}
	else
	{
		 $this->load->view('header');
         $this->load->view('add_brand');
	     $this->load->view('footer');	
	}		
  }
  function image_upload(){
      if($_FILES['b_img']['size'] != 0){
       // $upload_dir = './uploads/brand/';
     
        $config['upload_path']   = './uploads/brand/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|txt|docx';
     
        $config['max_size']  = '2048';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('b_img')){
            $this->form_validation->set_message('image_upload', $this->upload->display_errors());
            return false;
        }  
        else{
            $this->upload_data['file'] =  $this->upload->data();
            return true;
        }  
    }  
    else{
        $this->form_validation->set_message('image_upload', "No file selected");
        return false;
    }
}

	
	
	function edit_item($id,$image_id)
	{
		

		$this->load->model('item_model');
		$da['brand']=$this->item_model->update_product($id);
		$da['media']=$this->item_model->get_id($image_id);
		
	    $this->load->view('header');
	    $this->load->view('edit_brand',$da);
	    $this->load->view('footer');	
	

	}
	function up_item()
	{   
		//$this->form_validation->set_rules('brand_name','Name','required');
		$this->form_validation->set_rules('b_img', 'Profile Image', 'callback_image_upload');
		

		if($this->form_validation->run() == TRUE)
		{
		$this->load->helper(array('form', 'url','file'));
		//$this->load->helper(array('form', 'url'));
			 $config['upload_path'] = './uploads/brand';
		//$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|txt|docx';//txt,doc,docx,pdf,png,jpeg,jpg,gif

		$config['max_size']      = 2048;//MB
		$this->load->library('upload', $config);

        if (!$this->upload->do_upload('b_img'))
		 {
		$error =$this->upload->display_errors();
		$img_path=$this->input->post('b_img');							
		} else {
		
        	$file_data=$this->upload->data();
        	//$da=base_url().'/uploads/'.$file_data['file_name'];//url
			//$da1='/uploads/'.$file_data['file_name'];//path
			//$da2=$file_data['file_name'];//file name
        	$img_path='/uploads/brand/'.$file_data['file_name'];//path
			}
 $mc_id = $this->input->post('hid');
$today = date("Y-m-d");

			$data	=	array(
			'posted_date'=>$today,
			'm_img'=> $img_path,

			 );


		$this->load->model('item_model');
		$image_id=$this->item_model->add_image($data);
		if(empty($image_id)){
			

      $datas	=	array('brand_name'=>$this->input->post('brand_name'),
			              'updated_date'=>$today,	
			              //'b_img'=>$image_id,		              
			                );
  }
  else
  {
  	$datas	=	array('brand_name'=>$this->input->post('brand_name'),
			              'updated_date'=>$today,	
			              'b_img'=>$image_id,		              
			                );
  }
		$this->load->model('item_model');
		$this->item_model->updateit_item($mc_id,$datas);
		//$this->item_model->updateit_media($mc_id,$img_path);
		
		
		redirect('brand');	
			
	}
	else
	{
		$mc_id = $this->input->post('hid');
		$this->load->model('item_model');
		$da['brand']=$this->item_model->update_product($mc_id);

		
	    $this->load->view('header');
	    $this->load->view('edit_brand',$da);
	    $this->load->view('footer');		
	}
}
	function delete_item($id)
	{
		$this->load->model('item_model');
		$this->item_model->delete_item($id);		
		redirect('brand');
	}
	
	
	

}