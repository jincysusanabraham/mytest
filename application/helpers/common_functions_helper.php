<?php
function human_time($time = false){
	if(!$time){
		$time = time();
	}
	//$human_datetime = unix_to_human($time);
	$human_datetime = date('d / M  / Y',$time);
	//$human_datetime = date('dS M , Y',$time);
	return $human_datetime;
}
function human_time2($time = false){
	if(!$time){
		$time = time();
	}
	//$human_datetime = unix_to_human($time);
	$human_datetime = date('d / M  / Y h:i A',$time);
	//$human_datetime = date('dS M , Y',$time);
	return $human_datetime;
}
function human_time_newsletter($time = false){
	if(!$time){
		$time = time();
	}
	//$human_datetime = unix_to_human($time);
	$human_datetime = date('F  Y',$time);
	//$human_datetime = date('dS M , Y',$time);
	return $human_datetime;
}
function human_time_newsletter_view($time = false){
	if(!$time){
		$time = time();
	}
	//$human_datetime = unix_to_human($time);
	$human_datetime = date('Y-m-d',$time);
	//$human_datetime = date('dS M , Y',$time);
	return $human_datetime;
}

//2013-11-22
function human_time_news($time = false){
	if(!$time){
		$time = time();
	}
	//$human_datetime = unix_to_human($time);
	$human_datetime = date('M  d',$time);
	//$human_datetime = date('dS M , Y',$time);
	return $human_datetime;
}
	 function get_field_data($table,$condition=array(),$field_name) {
		
		$ci =&  get_instance();
		$ci->load->database();
		$ci->db->select($field_name);
		$ci->db->from($table);
		$ci->db->where($condition);
		$query =$ci->db->get();
		if($query->num_rows()>0)
			{
       $firstname=$query->row();	
			return $firstname->$field_name;
		}
		else
		{
		return 0;
		}
		
    }
		 function get_field_data_checkout($table,$condition=array(),$field_name) {
		
		$temp='NA';
		$ci =&  get_instance();
		$ci->load->database();
		$ci->db->select($field_name);
		$ci->db->from($table);
		$ci->db->where($condition);
		$query =$ci->db->get();
		if($query->num_rows()>0)
			{
       $firstname=$query->row();	
			return $firstname->$field_name;
		}
		else
		{
		return $temp;
		}
		
    }

		 function get_count_data($table,$condition=array(),$field_name) {
		
		$ci =&  get_instance();
		$ci->load->database();
		$ci->db->select($field_name);
		$ci->db->from($table);
		$ci->db->where($condition);
		$query =$ci->db->get();
		return $query->num_rows();
		
    }
	function get_all($table,$condition=array(),$order_by_fieled='',$order_by_type="asc",$limit=0,$row=0)
		{		
					
			$ci =&  get_instance();
			$ci->load->database();	
		
			if($condition){
				$ci->db->where($condition);
			}
			if($order_by_fieled){	
			$ci->db->order_by($order_by_fieled,$order_by_type); 	
			}
			if($limit)
			{
					$query = $ci->db->get($table,$limit,$row);
			}
			
			else
			{
			
			$query=$ci->db->get($table);
			}
			if($query->num_rows()>0){
					return $query->result_array();
				}
				else{
					return array();
				}
		}	

	
?>