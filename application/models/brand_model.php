<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class brand_model extends CI_Model {
var $table='brand';

		public function __construct() {
			parent::__construct();
			$this->load->database();
		}
	   function edit($condition=array(),$data_array=array()){
	
		if($condition){
			$this->db->where($condition);
		}
		if($this->db->update($this->table,$data_array)){
				return true;
			}
			else{
				return false;
				}
		}
	
		 function get_all_entries($row,$limit,$condition=array(),$order_by_fieled,$order_by_type="asc")
		{   
			//$this->db->where('blog_status','E');
			if($condition)
			{
			$this->db->where($condition);
			}
			if($order_by_fieled){	
			$this->db->order_by($order_by_fieled,$order_by_type); 	
			}
			$query = $this->db->get($this->table,$limit,$row);
			
			if ($query->num_rows() > 0){
			  return $query->result_array();
			}
			 else {
			  return array();
			}
	
		}
		
		function get_all($condition=array(),$order_by_fieled='',$order_by_type="asc",$limit=0)
		{			
			$this->db->from($this->table);
			if($condition){
				$this->db->where($condition);
			}
			if($order_by_fieled){	
			$this->db->order_by($order_by_fieled,$order_by_type); 	
			}
			if($limit){
			$this->db->limit($limit);
			}
			
			$query=$this->db->get();
			if($query->num_rows()>0){
			
					return $query->result_array();
					
				
				}
				else{
					return array();
				}
		}		
		function countrows($condition=array()){
		
			$this->db->from($this->table);
			if($condition){
					$this->db->where($condition);
				}
			$query = $this->db->get();
			$row=$query->num_rows();
			return $row;
		}
		
		function add_data($data_array=array())
	{
		
	  $this->db->insert('brand',$data_array);
	  return $this->db->insert_id();
		
	}
		function add_image($data_array=array())
	{
		
	  $this->db->insert('medias',$data_array);
	 $insertId = $this->db->insert_id();
    return  $insertId;
		
	}
		 function get_entry($condition=array()){
		 
			 $this->db->from($this->table);
			 if($condition)
				{
				$this->db->where($condition);
				}
			  	$query = $this->db->get();
			  if ($query->num_rows() == 1)
			  {
				return $query->result_array();
				
			  }
			   else {
				return false; 
			  }
		}
		function view_list()
	{
	    $brand=$this->db->get('brand');
		return $brand->result();
	}
	function get_media()
	{
	    $media=$this->db->get('medias');
		return $media->result();
	}
	function get_id($id)
	{
		$this->db->where('i_id',$id);
	    $media=$this->db->get('medias');
		return $media->result();
	}
	function view_more($food_desbrand_id)
	{
		$this->db->where('brand_id',$food_desbrand_id);
	    $brand=$this->db->get('brand');
		return $brand->result();
	}
	function view_type_more($type)
	{
		$this->db->where('t_type',$type);
	    $brand=$this->db->get('brand');
		return $brand->result();
	}
	function update_product($food_desbrand_id)
	{
	    
		$this->db->where('brand_id',$food_desbrand_id);
		$brand=$this->db->get('brand');
		return $brand->result();
	}
	function updateit_item($mbrand_id,$datas)
	{
		
		
	    
		$this->db->where('brand_id',$mbrand_id);
		$this->db->update('brand',$datas);
	
	}
	function updateit_media($id,$img_path)
	{
		//$data['m_img']=$img_path;
		$data['m_img']=$img_path;

		$this->db->where('i_id',$id);
		$this->db->update('medias',$data);
	}
	function delete_item($food_desbrand_id)
	{
		$this->db->where('brand_id',$food_desbrand_id);
		$this->db->delete('brand');
	}
		
		function get_max($field_name)
		{
			
			
			$this->db->select_max($field_name);
			$query = $this->db->get($this->table);
		}
}
?>
