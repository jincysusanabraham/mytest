<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class media_model extends CI_Model {
var $table='medias';

		public function __construct() {
			parent::__construct();
			$this->load->database();
		}
	  
	
		 function get_all_entries($row,$limit,$condition=array(),$order_by_fieled,$order_by_type="asc")
		{   
			//$this->db->where('blog_status','E');
			if($condition)
			{
			$this->db->where($condition);
			}
			if($order_by_fieled){	
			$this->db->order_by($order_by_fieled,$order_by_type); 	
			}
			$query = $this->db->get($this->table,$limit,$row);
			
			if ($query->num_rows() > 0){
			  return $query->result_array();
			}
			 else {
			  return array();
			}
	
		}
				
		function countrows($condition=array()){
		
			$this->db->from($this->table);
			if($condition){
					$this->db->where($condition);
				}
			$query = $this->db->get();
			$row=$query->num_rows();
			return $row;
		}
		
	
		
		
		function get_max($field_name)
		{
			
			
			$this->db->select_max($field_name);
			$query = $this->db->get($this->table);
		}
}
?>
