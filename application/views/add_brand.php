 
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
         <div>
    <ul class="breadcrumb">
        <li>
            <a href="">Dashboard</a>
        </li>
        <li>
            <a href="<?=base_url()?>brand">Brand</a>
        </li>
         <li>
            <a href="#">  Add Brand</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well1">
                <h2> Add Brand </h2>

               
            </div>
            <div class="box-content row">
        <div class="panel-body">
        <div class="form">

                <!--        error msg -->
<?php 
            if(!empty($error)){
               // echo $error_message;
                ?> <div class="alert alert-success  alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<?php echo $error; ?>  </div>
<?php } ?> 
          
      

<?php echo form_open_multipart('brand/add', array('id' => 'signupForm','class' => 'cmxform form-horizontal adminex-form')); ?>
                              
                  
                   <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-2">Brand Name</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" name="brand_name" type="text" value="<?=set_value('brand_name')?>"  />
                                            <h5 style="color:red;"> <strong> <?php echo form_error('brand_name'); ?> </strong></h5> 
                                        </div>
                                        
                                    </div>
                                    
                        
                                    
                                   

                                    <div class="form-group ">
              <label for="curl" class="control-label col-lg-2">Brand Image </label>              
                <div class="col-lg-10">
               <input type="file" id="exampleInputFile2" name="b_img" />
               <h5 style="color:red;"> <strong> <?php echo form_error('b_img'); ?> </strong></h5> 
                </div>
                </div>
                                     
                  
                                 
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                            <a href="" onclick="window.history.go(-1); return false;">  <button class="btn btn-default" type="button">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>

                            </div>
        </div>
              
                <!-- Ads end -->

            </div>
        </div>
    </div>
</div>



</div>
        <!--/span-->
        <!-- left menu ends -->
    </div><!-- dashboard -->
