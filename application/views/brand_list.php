
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
         <div>
    <ul class="breadcrumb">
        <li>
            <a href="">Dashboard</a>
        </li>
        <li>
            <a href="<?=base_url()?>brand">Brand</a>
        </li>
         <li>
            <a href="#"> Brand List</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well1">
                <h2> Brand List</h2>

               
            </div>
            <div class="box-content row">
               <header class="panel-heading">
            
            <span class="tools pull-right">
                <a href="<?php echo base_url(); ?>brand/show_add"><strong>Add Brand</strong></a><div>
             </span>
        </header>
        <div class="panel-body wrapper">
        <div class="adv-table" >
        
        <!-- // -->
       <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
        <tr><th width="5%">No</th>
                                         
                                              
                                              <th><strong><center>Brand Name</center>   </strong></th>
                                               <th><strong><center>Image</center>   </strong></th> 
                                              <th><strong><center>Posted Date</center>   </strong></th>
                                            
                                            <!--   <th><strong><center>Description</center>   </strong></th> -->
                                                       <th class="hidden-phone"><strong>View/Edit</strong></th>
                                                  <th class="hidden-phone"><strong>Delete</strong></th>
                                          
                                          </tr>
        </thead>
        <tbody>
  <?php
  $i=$row;
                          
  foreach($brand as $result){ 
      $img_id=$result['b_img'];                           
  ?>
        <?php 
                          
            foreach($media as $t)
            {                
     $arr= explode(" ",$img_id); 
     if(in_array($t->i_id,$arr))
      { 
       $image=$t->m_img;
       $image_id=$t->i_id;
      } 

              } ?>                                                               
        <tr class="gradeX">
             <td align="center"><?php echo $i+1; ?></td>

                                        
                                       <td ><?php echo $result['brand_name']; ?></td>
    <td ><center>    <img src="<?php echo base_url(); ?><?php echo $image; ?>" height="150" width="150"  /></center></td>
 
                                                    <td ><?php echo $result['created_date']; ?></td>
                                               
                                                  <!--   <td ><?php echo $result['b_desc']; ?></td> -->
 <td class="center hidden-phone" align="center"><a href="<?php echo base_url(); ?>brand/edit_item/<?php echo $result['brand_id']; ?>/<?php echo $result['b_img'];; ?>" title="Edit"><img src="<?php echo base_url(); ?>assets_admin/images/multi-edit.png" border="0"></a></td>

<td class="center hidden-phone" align="center"><a href="<?php echo base_url(); ?>brand/delete_item/<?php echo $result['brand_id']; ?>" title="delete" onClick="return confirm('Do you want to delete this record ?');"><img src="<?php echo base_url(); ?>assets_admin/images/delete.png" border="0"></a></td>
                                                   
                                          
                                                </tr>
                                            
    
    


                          <?php
  $i++;
                      
                        } ?>
                                                
     
        </tbody>
        
        </table>

        </div>
        </div>
              
                <!-- Ads end -->

            </div>
        </div>
    </div>
</div>



</div>
        <!--/span-->
        <!-- left menu ends -->
    </div><!-- dashboard -->
    