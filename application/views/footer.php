  <!--footer section start-->
        <footer>
            2020 &copy; Codetest 
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>assets_admin/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets_admin/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets_admin/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets_admin/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets_admin/js/jquery.stepy.js"></script>
<script src="<?php echo base_url(); ?>assets_admin/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets_admin/js/modernizr.min.js"></script>
<script src="<?php echo base_url(); ?>assets_admin/js/jquery.nicescroll.js"></script>


<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?=base_url()?>assets_admin/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets_admin/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="<?=base_url()?>assets_admin/js/dynamic_table_init.js"></script>




<!--form validation-->	  
<script type="text/javascript" src="<?=base_url()?>assets_admin/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>assets_admin/js/validation-init.js"></script>


<!--Calendar-->
<script src="<?=base_url()?>assets_admin/js/calendar/clndr.js"></script>
<script src="<?=base_url()?>assets_admin/js/calendar/evnt.calendar.init.js"></script>
<script src="<?=base_url()?>assets_admin/js/calendar/moment-2.2.1.js"></script>
<script src="<?=base_url()?>assets_admin/http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>

<!--common scripts for all pages-->
<script src="<?=base_url()?>assets_admin/js/scripts.js"></script>

<script type="text/javascript">
  // When the document is ready set up our sortable with it's inherant function(s)


$(document).ready(function(){   
    // Return a helper with preserved width of cells
    var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
    };

    $('#sortable-todo').sortable({
        update: function() {
            var stringDiv = "";
            $("#sortable-todo").children().each(function(i) {
                var li = $(this);
                stringDiv += " "+li.attr("id") + '=' + i + '&';
            });

            $.ajax({
                type: "POST",
                url: "<?=base_url()?>admin/todo/updateOrder",
                data: stringDiv
            }); 
        }
    }); 
    $( "#sortable-todo" ).disableSelection();    
});

</script>

</body>
</html>