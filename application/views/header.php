<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="utf-8">
    <title>Codetest</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- The styles -->
    <link id="bs-css" href="<?=base_url()?>admin_assets/css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link href='<?=base_url()?>admin_assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>

     <link href="<?=base_url()?>admin_assets/css/charisma-app.css" rel="stylesheet">
    <!-- <link href='<?=base_url()?>admin_assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
<link href='<?=base_url()?>admin_assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?=base_url()?>admin_assets/bower_components/chosen/chosen.min.css' rel='stylesheet'> -->
   <!--  <link href='<?=base_url()?>admin_assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'> -->
    <link href='<?=base_url()?>admin_assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>

    <!-- <link href='<?=base_url()?>admin_assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'> -->
  <!--   <link href='<?=base_url()?>admin_assets/css/jquery.noty.css' rel='stylesheet'> -->
   <!--  <link href='<?=base_url()?>admin_assets/css/noty_theme_default.css' rel='stylesheet'> -->
   <!--  <link href='<?=base_url()?>admin_assets/css/elfinder.min.css' rel='stylesheet'> -->
    <!-- <link href='<?=base_url()?>admin_assets/css/elfinder.theme.css' rel='stylesheet'> -->
    <!-- <link href='<?=base_url()?>admin_assets/css/jquery.iphone.toggle.css' rel='stylesheet'> -->
    <!-- <link href='<?=base_url()?>admin_assets/css/uploadify.css' rel='stylesheet'> -->
 <!--    <link href='<?=base_url()?>admin_assets/css/animate.min.css' rel='stylesheet'>  -->

 <!--icheck-->
  <link href="<?=base_url()?>assets_admin/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="<?=base_url()?>assets_admin/js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="<?=base_url()?>assets_admin/js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="<?=base_url()?>assets_admin/js/iCheck/skins/square/blue.css" rel="stylesheet">


   <link href="<?=base_url()?>assets_admin/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="<?=base_url()?>assets_admin/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?=base_url()?>assets_admin/js/data-tables/DT_bootstrap.css" />

  <!--dashboard calendar-->
  <link href="<?=base_url()?>assets_admin/css/clndr.css" rel="stylesheet">

  <!--Morris Chart CSS -->
  <link rel="stylesheet" href="<?=base_url()?>assets_admin/js/morris-chart/morris.css">

  <!--common-->
  <link href="<?=base_url()?>assets_admin/css/style.css" rel="stylesheet">
  <link href="<?=base_url()?>assets_admin/css/style-responsive.css" rel="stylesheet">
   <link href="<?=base_url()?>assets_admin/css/table-responsive.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="<?=base_url()?>assets_admin/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets_admin/css/bootstrap.css" rel="stylesheet">

    <!-- jQuery -->
   <!--   <link href='<?=base_url()?>admin_assets/bower_components/responsive-tables/responsive-tables.js' rel='stylesheet'> -->
    <script src="<?=base_url()?>admin_assets/bower_components/responsive-tables/responsive-tables.js"></script>
<script src="<?=base_url()?>admin_assets/bower_components/jquery/jquery.min.js"></script>
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?=base_url()?>admin_assets/img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation" style="height:67px;">

        <div class="navbar-inner" >
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a  href="#"> <div class="navbar-brand1"><img style="width: 50px;height: 50px;"  alt="Charisma Logo" src="<?=base_url()?>assets_admin/images/roundlogo.png" class="hidden-xs"/></div>
             <div class="navbar-brand">   <span style="width: 15px" ><center>Codetest</center></span></div></a>

            <!-- user dropdown starts -->

            <div class="btn-group pull-right" style="padding-top: 8px;">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-tint"></i><span class="hidden-sm hidden-xs"> Codetest</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="">Change Password</a></li>
                    <li class="divider"></li>
                    <li><a href="">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts <span >SHOP4MARINERS</span></a> -->
            <!-- theme selector ends -->

            <ul class="collapse navbar-collapse nav navbar-nav top-menu" style="padding-left: 120px">
                <li><a href=""><i class="fa fa-globe"></i> Visit</a></li>
             <!--    <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="fa fa-star"></i> Dropdown <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li> -->
                <li>
                    <form class="navbar-search pull-left" style="padding-top: 8px">
                        <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                               type="text">
                    </form>
                </li>
            </ul>

        </div>
    </div>
        <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2" style="width:453;height: 700;"><!--style="position: fixed;" <div style="width:453;height: 594;background-color: #30a9ef"> -->
            <div class="sidebar-nav"  >
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                  
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header"></li>
                        <li><a class="ajax-link" href="<?=base_url()?>brand/show_add"><img src="https://img.icons8.com/color/20/000000/cottage.png"><span> Add Brand</span></a>
                        </li>
                        <li><a class="ajax-link" href="<?=base_url()?>brand"><img src="https://img.icons8.com/color/20/000000/edit-user-male.png"><span>Brand list</span></a>
                        </li>
                        <li><a class="ajax-link" href="<?=base_url()?>media"><img src="https://img.icons8.com/color/20/000000/edit-user-male.png"><span> Media History </span></a>
                        </li>
                        
                        
                       
                        

                    </ul>
                    <!-- <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label> -->
                </div>
            </div>
        </div>