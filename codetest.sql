-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2020 at 12:44 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codetest`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(100) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `b_img` varchar(20) DEFAULT NULL,
  `b_desc` text,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_name`, `created_date`, `b_img`, `b_desc`, `updated_date`) VALUES
(1, 'brand1', '2020-05-26', '8', NULL, '2020-05-26'),
(2, 'brand2', '2020-05-26', '2', NULL, NULL),
(3, 'brand3', '2020-05-26', '3', NULL, NULL),
(4, 'brand4', '2020-05-26', '5', NULL, '2020-05-26'),
(6, 'text', '2020-05-26', '10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bulk_order`
--

CREATE TABLE `bulk_order` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `emailid` varchar(100) NOT NULL,
  `mob` varchar(100) NOT NULL,
  `bulkOrder` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulk_order`
--

INSERT INTO `bulk_order` (`id`, `user_id`, `name`, `emailid`, `mob`, `bulkOrder`) VALUES
(1, '', 'dd', 'dg', 'sdddddddddddddd', 'ddddv'),
(2, '$u_id', '$name', '$email', '$phone', '$comment'),
(3, '8', 'cv', 'ccccccccccv', 'c9666', 'dv'),
(4, '8', 'rr', 'rr', '453', 'dfgg'),
(5, '', 'muhzin', 'muhzin@gmail.com', '9961630484', 'SUGAR 1KG, APPLE WATCH 12NOS'),
(6, '83', 'Lakshmi', 'rajalakshmi9568@gmail.com', '7561807715', 'Agga,vabaj,vajaj,gaiakj,bakaigwc,vajaia'),
(7, '101', 'Rajalakshmi R Nair', 'rajalakshmirnair29@gmail.com', '7561807715', 'Phone 1\nPhone 2\nCameras\nTv');

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `i_id` int(11) NOT NULL,
  `m_img` text NOT NULL,
  `posted_date` date NOT NULL,
  `m_thumb` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`i_id`, `m_img`, `posted_date`, `m_thumb`) VALUES
(1, '/uploads/brand/21.png', '2020-05-26', ''),
(2, '/uploads/brand/11.png', '2020-05-26', ''),
(3, '/uploads/brand/61.png', '2020-05-26', ''),
(4, '/uploads/brand/81.png', '2020-05-26', ''),
(5, '/uploads/brand/101.png', '2020-05-26', ''),
(6, '/uploads/brand/31.png', '2020-05-26', ''),
(7, '/uploads/brand/41.png', '2020-05-26', ''),
(8, '/uploads/brand/51.png', '2020-05-26', ''),
(9, '/uploads/brand/121.png', '2020-05-26', ''),
(10, '/uploads/brand/hello1.txt', '2020-05-26', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `bulk_order`
--
ALTER TABLE `bulk_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`i_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bulk_order`
--
ALTER TABLE `bulk_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
